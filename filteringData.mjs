
const idsOfGlass = (data) => {
    let ids = data.filter(eachData => {
        if (eachData.material.includes("Glass") || eachData.material.includes("glass")) {
            return eachData.id
        }
    })
    return ids
}

const increasePlasticQtyBy5 = (data) => {
    let increasedQty = data.filter(eachData => eachData.material === "Plastic")
        .map(eachData => {
            eachData.quantity += 5
            return eachData
        })

    return increasedQty
}


export { increasePlasticQtyBy5, idsOfGlass }