
const findNumberOfEachColors = (data) => {
    return data.reduce((groupedColors, nextColor) => {
        if (!groupedColors[nextColor.color]) {
            groupedColors[nextColor.color] = 1
        }
        else {
            groupedColors[nextColor.color] += 1
        }
        return groupedColors

    }, {})
}

export default findNumberOfEachColors