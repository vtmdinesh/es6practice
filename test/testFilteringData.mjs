
// trying named exports by object destructuring using ES6


import fs from "fs"
import data from "../question.mjs"
import { increasePlasticQtyBy5, idsOfGlass } from "../filteringData.mjs"

const detailsOfId = idsOfGlass(data)
let resultData = JSON.stringify(detailsOfId) + "\n"

const increasedQty = increasePlasticQtyBy5(data)
resultData = resultData + "\n\n" + JSON.stringify(increasedQty)

fs.appendFile("../output/results.json", resultData, (err) => {
    if (err) {
        console.error(err)
    }
    else {
        console.log("File Written Successfully")
    }
})


