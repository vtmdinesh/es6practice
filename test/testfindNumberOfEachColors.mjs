import fs from "fs"
import data from "../question.mjs"
import findNumberOfEachColors from "../findNumberOfEachColors.mjs"

const details = findNumberOfEachColors(data)
let resultData = JSON.stringify(details) + "/n/n"


fs.writeFile("../output/results.json", resultData, (err) => {
    if (err) {
        console.error(err)
    }
    else {
        console.log("File Written Successfully")
    }
})


